<?php

namespace controladores;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Wsdl
 *
 * @author sebastian
 */
require_once 'conexion.php';
require_once 'config.php';
require_once 'nusoap/lib/nusoap.php';

class Servicio_Wsdl {

    //put your code here
    protected $conexion;

    public function __construct() {
        $this->conexion = new Conexion();
    }

    public function listar_medico($token) {
        if (!$this->validarToken($token)) {
            return array("message" => "No puede acceder a este recurso",
                "codigo" => "401", "persona" => "NP");
        } else {
            $obj = $this->conexion->conexion();
            $query = "select * from persona";
            $stmt = $obj->prepare($query);
            $stmt->execute();
            $datos = array();
            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                $data = array(
                    "external_id"=>$row["external_id"],
                    "persona"=>$row["apellidos"].' '.$row["nombres"],                     
                    "cedula"=>$row["cedula"], 
                    "correo"=>$row["correo"],
                    "especialidad"=>$row["especialidad"]);
                $datos[] = $data;
            }
            return $datos;
        }
    }
    
    public function listar_pacientes($token) {
        //echo $this->validarToken($token).' ****';
        if (!$this->validarToken($token)) {
            return array("message" => "No puede acceder a este recurso",
                "codigo" => "401", "persona" => "NP");
        } else {
            $obj = $this->conexion->conexion();
            $query = "select * from paciente inner join historial on historial.id_paciente = paciente.id";
            $stmt = $obj->prepare($query);
            $stmt->execute();
            $datos = array();
            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                $data = array("cedula"=>$row["cedula"], 
                    "apellidos"=>$row["apellidos"], 
                    "nombres"=>$row["nombres"],
                    "external"=>$row["external_id"],
                    "fecha_nac"=>$row["fecha_nac"],
                    "genero"=>$row["sexo"],
                    "direccion"=>$row["direccion"],
                    "celular"=>$row["celular"],
                    "telefono"=>$row["telefono"],
                    "habitos"=>$row["habitos"],
                    "enfermedades"=>$row["enfermedades"],
                    "enferm_heder"=>$row["enferm_heder"]);
                $datos[] = $data;
            }
            return $datos;
        }
    }

    public function obtener_paciente($token, $external) {
        //echo $this->validarToken($token).' ****';
        if (!$this->validarToken($token)) {
            return array("message" => "No puede acceder a este recurso",
                "codigo" => "401", "persona" => "NP");
        } else {
            $obj = $this->conexion->conexion();
            $query = "select * from paciente inner join historial on historial.id_paciente = paciente.id where external_id = :external";
            $stmt = $obj->prepare($query);
            $stmt->bindParam("external", $external, \PDO::PARAM_STR);
            $stmt->execute();
            $datos = array();
            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                $data = array("cedula"=>$row["cedula"], 
                    "apellidos"=>$row["apellidos"], 
                    "nombres"=>$row["nombres"],
                    "external"=>$row["external_id"],
                    "fecha_nac"=>$row["fecha_nac"],
                    "genero"=>$row["sexo"],
                    "direccion"=>$row["direccion"],
                    "celular"=>$row["celular"],
                    "telefono"=>$row["telefono"],
                    "habitos"=>$row["habitos"],
                    "enfermedades"=>$row["enfermedades"],
                    "enferm_heder"=>$row["enferm_heder"]);
                $datos = $data;
            }
            return $datos;
        }
    }
    
    public function especialidades() {
        $obj = $this->conexion->conexion();
        $query = "select * from especialidad";
        $stmt = $obj->prepare($query);
        $stmt->execute();
        $datos = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $datos[] = $row;
        }
        return $datos;
    }

    public function login($user, $password) {
        $obj = $this->conexion->conexion();
        $query = "select * from persona where correo = :cor limit 1";
        $stmt = $obj->prepare($query);
        $stmt->bindParam("cor", $user, \PDO::PARAM_STR);
        $stmt->execute();
        $datos = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {

            if ($row["clave"] == $password) {
                $datos["external_id"] = $row["external_id"];
                $datos["token"] = $row["token"];
                $datos["persona"] = $row["apellidos"] . ' ' . $row["nombres"];
            }
        }
        return $datos;
    }

    public function retornar_msg($message, $code) {
        return array("mensaje" => $message, "codigo" => $code);
    }

    public function obtenerConexion() {
        return $this->conexion->conexion();
    }

    public function guardar_pacientes($token, $data) {
        if (!$this->validarToken($token)) {
            return array("message" => "No puede acceder a este recurso",
                "codigo" => "401", "persona" => "NP");
        } else {
            $obj = $this->conexion->conexion();
            $external = $this->gen_uuid();
            $query = "INSERT INTO paciente (cedula, apellidos, nombres, external_id, sexo, edad, fecha_nac, direccion, telefono, celular) "
                    . "values(:ced, :ape, :nom, :external, :sex, :edad, :fecha, :dir, :fono, :celu)";
            $stmt = $obj->prepare($query);
            $stmt->bindParam("ced", $data['ced'], \PDO::PARAM_STR);
            $stmt->bindParam("ape", $data['ape'], \PDO::PARAM_STR);
            $stmt->bindParam("nom", $data['nom'], \PDO::PARAM_STR);
            $stmt->bindParam("external", $external, \PDO::PARAM_STR);
            $stmt->bindParam("sex", $data['sex'], \PDO::PARAM_STR);
            $stmt->bindParam("edad", $data['edad'], \PDO::PARAM_INT);
            $stmt->bindParam("fecha", $data['fecha'], \PDO::PARAM_STR);
            $stmt->bindParam("dir", $data['dir'], \PDO::PARAM_STR);
            $stmt->bindParam("fono", $data['fono'], \PDO::PARAM_STR);
            $stmt->bindParam("celu", $data['celu'], \PDO::PARAM_STR);

            $stmt->execute();
            $newId = $obj->lastInsertId();

            if ($newId > 0) {
                $obj = $this->conexion->conexion();
                $query = "INSERT INTO historial (habitos, enfermedades, enferm_heder, id_paciente) "
                        . "values(:hab, :enf, :enf_h, :id)";
                $stmt = $obj->prepare($query);
                $stmt->bindParam("hab", $data['hab'], \PDO::PARAM_STR);
                $stmt->bindParam("enf", $data['enf'], \PDO::PARAM_STR);
                $stmt->bindParam("enf_h", $data['enf_h'], \PDO::PARAM_STR);
                $stmt->bindParam("id", $newId, \PDO::PARAM_INT);
                $stmt->execute();
                return array("message" => "Se ha creado el paciente",
                    "codigo" => "200",
                    "persona" => $data['ape'] . ' ' . $data['nom']
                );
            } else {
                return array("message" => "No se pudo crear",
                    "codigo" => "500", "persona" => "NP");
            }
            
            
        }
    }
    
    public function modificar_paciente($token, $data) {
        if (!$this->validarToken($token)) {
            return array("message" => "No puede acceder a este recurso",
                "codigo" => "401", "persona" => "NP");
        } else {
            $obj = $this->conexion->conexion();
            
            $query = "UPDATE paciente set cedula = :ced, apellidos = :ape, "
                    . "nombres = :nom, sexo = :sex, edad = :edad, "
                    . "fecha_nac = :fecha, "
                    . "direccion = :dir, telefono = :fono, celular = :celu where external_id = :external";
            $stmt = $obj->prepare($query);
            $stmt->bindParam("ced", $data['ced'], \PDO::PARAM_STR);
            $stmt->bindParam("ape", $data['ape'], \PDO::PARAM_STR);
            $stmt->bindParam("nom", $data['nom'], \PDO::PARAM_STR);            
            $stmt->bindParam("sex", $data['sex'], \PDO::PARAM_STR);
            $stmt->bindParam("edad", $data['edad'], \PDO::PARAM_INT);
            $stmt->bindParam("fecha", $data['fecha'], \PDO::PARAM_STR);
            $stmt->bindParam("dir", $data['dir'], \PDO::PARAM_STR);
            $stmt->bindParam("fono", $data['fono'], \PDO::PARAM_STR);
            $stmt->bindParam("celu", $data['celu'], \PDO::PARAM_STR);
            $stmt->bindParam("external", $data['external'], \PDO::PARAM_STR);

            
            //$newId = $obj->lastInsertId();

            if ($stmt->execute()) {
                $historial = $this->getHistorial($data['external'], $token);
                $obj = $this->conexion->conexion();
                $query = "UPDATE historial set habitos = :hab, "
                        . "enfermedades = :enf, enferm_heder = :enf_h where id = :id ";
                        
                $stmt = $obj->prepare($query);
                $stmt->bindParam("hab", $data['hab'], \PDO::PARAM_STR);
                $stmt->bindParam("enf", $data['enf'], \PDO::PARAM_STR);
                $stmt->bindParam("enf_h", $data['enf_h'], \PDO::PARAM_STR);                
                $stmt->bindParam("id", $historial["id"], \PDO::PARAM_INT);                
                $stmt->execute();
                return array("message" => "Se ha modificado el paciente",
                    "codigo" => "200",
                    "persona" => $data['ape'] . ' ' . $data['nom']
                );
            } else {
                return array("message" => "No se pudo crear",
                    "codigo" => "500", "persona" => "NP");
            }
            
            
        }
    }

    public function guardar_medico($data) {
        $obj = $this->conexion->conexion();
        $external = $this->gen_uuid();
        $tokenAux = $data['correo'] . '::' . $external;
        $token = base64_encode($tokenAux);
        $query = "INSERT INTO persona (cedula, apellidos, nombres, registro, correo, clave, token, especialidad, external_id) "
                . "values(:ced, :ape, :nom, :reg, :cor, :cla, :tok, :espe, :external)";
        $stmt = $obj->prepare($query);
        $stmt->bindParam("ced", $data['ced'], \PDO::PARAM_STR);
        $stmt->bindParam("ape", $data['ape'], \PDO::PARAM_STR);
        $stmt->bindParam("nom", $data['nom'], \PDO::PARAM_STR);
        $stmt->bindParam("reg", $data['nro'], \PDO::PARAM_STR);
        $stmt->bindParam("cor", $data['correo'], \PDO::PARAM_STR);
        $stmt->bindParam("cla", $data['clave'], \PDO::PARAM_STR);
        $stmt->bindParam("tok", $token, \PDO::PARAM_STR);
        $stmt->bindParam("espe", $data['espe'], \PDO::PARAM_STR);
        $stmt->bindParam("external", $external, \PDO::PARAM_STR);
        $stmt->execute();
        $newId = $obj->lastInsertId();

        if ($newId > 0) {
            return array("external_id" => $external,
                "token" => $token,
                "persona" => $data['ape'] . ' ' . $data['nom']
            );
        } else {
            return array();
        }
    }

    public function verificar_cedula($ced) {
        $query = "select * from persona where cedula = '" . $ced . "'";
        $stmt = $this->conexion->conexion()->prepare($query);
        $stmt->execute();
        return $stmt->rowCount();        
    }
    
    public function verificar_cedula_paciente($ced) {
        $query = "select * from paciente where cedula = '" . $ced . "'";
        $stmt = $this->conexion->conexion()->prepare($query);
        $stmt->execute();
        return $stmt->rowCount();        
    }

    private function gen_uuid() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    function validarToken($token) {
        $tokenT = base64_decode($token);
        $tokenArreglo = explode("::", $tokenT);
        
        $obj = $this->conexion->conexion();
        $query = "select * from persona where correo = :cor limit 1";
        $stmt = $obj->prepare($query);
        
        $stmt->bindParam("cor", $tokenArreglo[0], \PDO::PARAM_STR);
        $stmt->execute();
        $verificar = false;
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            if ($row["external_id"] == $tokenArreglo[1]) {
                $verificar = true;
            }
        }
        return $verificar;
        //$data['correo'] . '::' . $external;
    }
    function getHistorial($external, $token) {
    //echo $this->validarToken($token).' ****';
        if (!$this->validarToken($token)) {
            return array("message" => "No puede acceder a este recurso",
                "codigo" => "401", "persona" => "NP");
        } else {
            $obj = $this->conexion->conexion();
            $query = "select historial.* from paciente inner join historial on historial.id_paciente = paciente.id where paciente.external_id = :external";
            $stmt = $obj->prepare($query);
            $stmt->bindParam("external", $external, \PDO::PARAM_STR);
            $stmt->execute();
            $datos = array();
            while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
                $data = array(
                    "id"=>$row["id"],
                    "habitos"=>$row["habitos"],
                    "enfermedades"=>$row["enfermedades"],
                    "enferm_heder"=>$row["enferm_heder"]);
                $datos = $data;
            }
            return $datos;
        }
    }
}
