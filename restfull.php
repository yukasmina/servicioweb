<?php

require_once './controladores/Servicio_Wsdl.php';
require_once './controladores/config.php';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, Content-Type, api-token');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
header('Content-type: application/json');
header("HTTP/1.1 200");
$uri = $_SERVER['REQUEST_URI'];
$dir = explode("restfull.php/", $uri);
if (count($dir) >= 2) {
    $tokenDir = explode("/", $dir[1]);
    //por lo general el primero es la accion
    $accion = $tokenDir[0];
    if ($accion == 'listar_paciente') {
        $token = obtener_header("api-token");
        listar_paciente($token);
    }
    if ($accion == 'guardar_paciente') {
        $token = obtener_header("api-token");
        registro_paciente($token);
    }
    if ($accion == 'modificar_paciente') {
        if (isset($tokenDir[1]) && $tokenDir[1] != "") {
            $token = obtener_header("api-token");
            modificar_paciente($token, $tokenDir[1]);
        } else {
            echo json_encode(array("message" => "Recurso no encontrado"));
            http_response_code(404);
        }
    }
    if ($accion == 'obtener_paciente') {
        if (isset($tokenDir[1]) && $tokenDir[1] != "") {
            $token = obtener_header("api-token");
            obtener_paciente($token, $tokenDir[1]);
        } else {
            echo json_encode(array("message" => "Recurso no encontrado"));
            http_response_code(404);
        }
    }
    if ($accion == 'obtener_historial') {
        if (isset($tokenDir[1]) && $tokenDir[1] != "") {
            $token = obtener_header("api-token");
            obtener_historial($token, $tokenDir[1]);
        } else {
            echo json_encode(array("message" => "Recurso no encontrado"));
            http_response_code(404);
        }
    }
} else {
    echo json_encode(array("message" => "Recurso no encontrado"));
    http_response_code(404);
}

function obtener_paciente($token, $external) {
    $wsdl = new controladores\Servicio_Wsdl();
    if ($token != '') {
        $lista = $wsdl->obtener_paciente($token, $external);
        echo json_encode($lista);
    } else {
        echo json_encode(array("message" => FT,
            "codigo" => "403", "persona" => "NP"));
    }
}

function obtener_historial($token, $external) {
    $wsdl = new controladores\Servicio_Wsdl();
    if ($token != '') {
        $lista = $wsdl->getHistorial($external, $token);
        echo json_encode($lista);
    } else {
        echo json_encode(array("message" => FT,
            "codigo" => "403", "persona" => "NP"));
    }
}

/*
  if (isset($_POST['accion']) && $_POST['accion'] === "registro_paciente") {
  $token = obtener_header("api-token");
  registro_paciente($token);

  }

  if (isset($_GET['accion']) && $_GET['accion'] === "listar_paciente") {
  $token = obtener_header("api-token");
  listar_paciente($token);

  }
 * 
 */

function listar_paciente($token) {
    $wsdl = new controladores\Servicio_Wsdl();
    if ($token != '') {
        $lista = $wsdl->listar_pacientes($token);
        //echo $lista["codigo"];
        if (isset($lista["codigo"])) {
            //   setResponseCode($lista["codigo"], $lista["message"]);            
        } else {
            // setResponseCode("200");                        
        }

        echo json_encode($lista);
    } else {
        //setResponseCode("401", FT);
        //http_response_code(200);

        echo json_encode(array("message" => FT,
            "codigo" => "401", "persona" => "NP"));
    }
}

function modificar_paciente($token, $external) {
    $wsdl = new controladores\Servicio_Wsdl();
    $json = file_get_contents('php://input');
    $datos = json_decode($json);
    if ($token != '') {

        if (strlen(trim($datos->cedula)) > 0 
                && strlen(trim($external)) > 0 
                && strlen(trim($datos->apellidos)) > 0 
                && strlen(trim($datos->nombres)) > 0 
                && strlen(trim($datos->fecha_nac)) > 0 
                && strlen(trim($datos->edad)) > 0 
                && strlen(trim($datos->genero)) > 0 
                && strlen(trim($datos->direccion)) > 0 
                && strlen(trim($datos->telefono)) > 0 
                && strlen(trim($datos->celular)) > 0 
                && strlen(trim($datos->habitos)) > 0 
                && strlen(trim($datos->enfermedades)) > 0 
                && strlen(trim($datos->enfermedades_hederitarias)) > 0) {
            $data['token'] = $token;
            $data['external'] = $external;
            $data['ced'] = $datos->cedula;
            $data['ape'] = $datos->apellidos;
            $data['nom'] = $datos->nombres;
            $data['sex'] = $datos->genero;
            $data['edad'] = $datos->edad;
            $data['fecha'] = $datos->fecha_nac;
            $data['dir'] = $datos->direccion;
            $data['fono'] = $datos->telefono;
            $data['celu'] = $datos->celular;
            $data['hab'] = $datos->habitos;
            $data['enf'] = $datos->enfermedades;
            $data['enf_h'] = $datos->enfermedades_hederitarias;
            $medico = $wsdl->modificar_paciente($token, $data);            
            echo json_encode($medico);
            //return $medico;
        } else {
            //    setResponseCode(400);
            echo json_encode(array("message" => FD,
                "codigo" => "400", "persona" => "NP"));
        }
    } else {
        //setResponseCode(401);
        echo json_encode(array("message" => FT,
            "codigo" => "401", "persona" => "NP"));
    }
}

/**
 * 
 * @return type Devuelve un mensaje en formato (message, codigo, persona)
 */
function registro_paciente($token) {
    $wsdl = new controladores\Servicio_Wsdl();
    $json = file_get_contents('php://input');
    $datos = json_decode($json);
    if ($token != '') {
        if ($wsdl->verificar_cedula_paciente($datos->cedula) == 0) {
            if (strlen(trim($datos->cedula)) > 0 && strlen(trim($datos->apellidos)) > 0 && strlen(trim($datos->nombres)) > 0 && strlen(trim($datos->fecha_nac)) > 0 && strlen(trim($datos->edad)) > 0 && strlen(trim($datos->genero)) > 0 && strlen(trim($datos->direccion)) > 0 && strlen(trim($datos->telefono)) > 0 && strlen(trim($datos->celular)) > 0 && strlen(trim($datos->habitos)) > 0 && strlen(trim($datos->enfermedades)) > 0 && strlen(trim($datos->enfermedades_hederitarias)) > 0) {
                $data['token'] = $token;
                $data['ced'] = $datos->cedula;
                $data['ape'] = $datos->apellidos;
                $data['nom'] = $datos->nombres;
                $data['sex'] = $datos->genero;
                $data['edad'] = $datos->edad;
                $data['fecha'] = $datos->fecha_nac;
                $data['dir'] = $datos->direccion;
                $data['fono'] = $datos->telefono;
                $data['celu'] = $datos->celular;
                $data['hab'] = $datos->habitos;
                $data['enf'] = $datos->enfermedades;
                $data['enf_h'] = $datos->enfermedades_hederitarias;
                $medico = $wsdl->guardar_pacientes($token, $data);

                //  setResponseCode($medico["codigo"], $medico["message"]);
                echo json_encode($medico);
                //return $medico;
            } else {
                //    setResponseCode(400);
                echo json_encode(array("message" => FD,
                    "codigo" => "400", "persona" => "NP"));
            }
        } else {
            //  setResponseCode(400);
            echo json_encode(array("message" => CR,
                "codigo" => "400", "persona" => "NP"));
        }
    } else {
        //setResponseCode(401);
        echo json_encode(array("message" => FT,
            "codigo" => "401", "persona" => "NP"));
    }
}

function verToken() {
    $token = "";
    if (isset($_GET["api-token"]))
        $token = $_GET["api-token"];
    if (isset($_POST["api-token"]))
        $token = $_POST["api-token"];
    return $token;
}

function obtener_header($nombreToken) {
    $valorToken = "";
    foreach (getallheaders() as $nombre => $valor) {

        if ($nombre == $nombreToken)
            $valorToken = $valor;
    }
    $headers = apache_request_headers();
    foreach ($headers as $nombre => $valor) {
        if ($nombre == $nombreToken)
            $valorToken = $valor;
//        echo "$nombre: $valor\n";
    }
    return $valorToken;
}

function setResponseCode($cod, $reason = null) {
    $code = intval("200");
    http_response_code($code);
    if (version_compare(phpversion(), '5.4', '>') && is_null($reason))
        http_response_code($code);
    else
        header("HTTP/1.1 " . $code . "");
    //header(':', true, $code);
    //header('X_PHP_Response_Code: '.$code, true, $code);
    //header(trim("HTTP/1.0 $code $reason"));
    //http_response_code($code);
    header('Status: ' . $code);
}
