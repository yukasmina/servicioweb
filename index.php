<?php

require_once './controladores/Servicio_Wsdl.php';
require_once './controladores/conexion.php';
require_once './controladores/config.php';
require_once './nusoap/lib/nusoap.php';

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Content-type: application/xml');

$server = new \soap_server();
$server->configureWSDL('server', 'urn:server');

$server->wsdl->schemaTargetNamespace = 'urn:server';

$server->wsdl->addComplexType(
        'especialidad',
        'complexType',
        'struct',
        'all',
        '',
        array('id' => array('name' => 'id',
                'type' => 'xsd:int'),
            'nombre' => array('name' => 'nombre',
                'type' => 'xsd:string'))
);

$server->wsdl->addComplexType(
        'acceso',
        'complexType',
        'struct',
        'all',
        '',
        array('external_id' => array('name' => 'external_id',
                'type' => 'xsd:string'),
            'token' => array('name' => 'token',
                'type' => 'xsd:string'),
            'persona' => array('name' => 'persona',
                'type' => 'xsd:string'))
);

$server->wsdl->addComplexType(
        'medico',
        'complexType',
        'struct',
        'all',
        '',
        array('external_id' => array('name' => 'external_id',
                'type' => 'xsd:string'),
            'persona' => array('name' => 'persona',
                'type' => 'xsd:string'),
            'cedula' => array('name' => 'cedula',
                'type' => 'xsd:string'),
            'correo' => array('name' => 'correo',
                'type' => 'xsd:string'),
            'especialidad' => array('name' => 'especialidad',
                'type' => 'xsd:string'))
);
$server->wsdl->addComplexType('lista_medicos', 'complexType', 'array', '',
        'SOAP-ENC:Array', array(),
        array(array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:medico[]')),
        'tns:medico');

$server->wsdl->addComplexType('lista_especialidades', 'complexType', 'array', '',
        'SOAP-ENC:Array', array(),
        array(array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:especialidad[]')),
        'tns:especialidad');

$server->register('lista_especialidades',
        array(), // parameter
        array('return' => 'tns:lista_especialidades'), // output  array('return' => 'xsd:string'),     // output
        'urn:server', // namespace
        'urn:server#helloServer', // soapaction
        'rpc', // style
        'encoded', // use
        'Permite listar todas las especialidades');                   // description

// Register the "hello" method to expose it
$server->register('iniciar_sesion',
        array('usuario' => 'xsd:string', 'clave' => 'xsd:string'), // parameter
        array('return' => 'tns:acceso'), // output  array('return' => 'xsd:string'),     // output
        'urn:server', // namespace
        'urn:server#helloServer', // soapaction
        'rpc', // style
        'encoded', // use
        'Permite iniciar sesion');                   // description

$server->register('medicos_listado',
        array('token' => 'xsd:string'), // parameter
        array('return' => 'tns:lista_medicos'), // output  array('return' => 'xsd:string'),     // output
        'urn:server', // namespace
        'urn:server#medicos', // soapaction
        'rpc', // style
        'encoded', // use
        'Permite listar medicos');                   // description

$server->register('registro_medico',
        array('cedula' => 'xsd:string', 'apellidos' => 'xsd:string', 'nombres' => 'xsd:string',
            'especialidad' => 'xsd:string', 'correo' => 'xsd:string',
            'nro_reg' => 'xsd:string', 'clave' => 'xsd:string'), // parameter
        array('return' => 'tns:acceso'), // output  array('return' => 'xsd:string'),     // output
        'urn:server', // namespace
        'urn:server#helloServer', // soapaction
        'rpc', // style
        'encoded', // use
        'Permite registrar medicos');                   // description

/**
 * Permite iniciar sesion
 * @param type $username Usuario
 * @param type $password Clave
 * @return \soap_fault Objeto Acceso
 */
function iniciar_sesion($username, $password) {
    $wsdl = new controladores\Servicio_Wsdl();
    if (strlen(trim($username)) > 0 && strlen(trim($password)) > 0) {
        $login = $wsdl->login($username, $password);
        if (count($login) > 0) {
            return $login;
        } else {
            return new soap_fault('500', '', NEC, '');
        }
    } else {
        return new soap_fault('500', '', FD, '');
    }
}
/**
 * Lista todos los medicos
 * @param type $token El token de autorizacion
 * @return array Un arreglo de medicos
 */
function medicos_listado($token) {
    $wsdl = new controladores\Servicio_Wsdl();
    if (strlen(trim($token))) {
        $datos = $wsdl->listar_medico($token);
        if (!isset($datos["codigo"])) {
            return $datos;
        } else {
            return new soap_fault('500', '', NA, '');
        }
    } else {
        return new soap_fault('500', '', FT, '');
    }
    //$data = array();
    //return $data;
}
/**
 * Registra un medico
 * @param type $cedula Es el DNI
 * @param type $apellidos Apellidos
 * @param type $nombres Nombres
 * @param type $especialidad Especialidad del medico
 * @param type $correo Correo electronico debe ser unico
 * @param type $nro_reg Nro de registro
 * @param type $clave Clave
 * @return \soap_fault Un objeto medico
 */
function registro_medico($cedula, $apellidos, $nombres,
        $especialidad, $correo, $nro_reg, $clave) {
    if (preg_match("/^[N]-[0-9]{4}-[R]-[0-9]{3}$/", $nro_reg)) {
        $wsdl = new controladores\Servicio_Wsdl();
        if ($wsdl->verificar_cedula($cedula) == 0) {
            if (strlen(trim($cedula)) > 0 && strlen(trim($apellidos)) > 0 && strlen(trim($nombres)) > 0 && strlen(trim($especialidad)) > 0 && strlen(trim($nro_reg)) > 0 && strlen(trim($clave)) > 0 && strlen(trim($correo)) > 0) {
                $data['ced'] = $cedula;
                $data['ape'] = $apellidos;
                $data['nom'] = $nombres;
                $data['espe'] = $especialidad;
                $data['nro'] = $nro_reg;
                $data['clave'] = $clave;
                $data['correo'] = $correo;
                $medico = $wsdl->guardar_medico($data);
                if (count($medico) > 0) {
                    return $medico;
                } else {
                    return new soap_fault('500', '', NSPR, '');
                }
            } else {
                return new soap_fault('500', '', FD, '');
            }
        } else {
            return new soap_fault('500', '', CR, '');
        }
    } else {
        return new soap_fault('500', '', NRI . ' ' . $nro_reg, '');
    }
}

function lista_especialidades() {
    $wsdl = new controladores\Servicio_Wsdl();
    return $wsdl->especialidades();
}

$server->service(file_get_contents("php://input"));
